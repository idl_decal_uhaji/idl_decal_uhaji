function kill_directional, value, A, k
;This function takes in a value and then uses a
;mathematical function to determine the probability of death. The
;death probability function can be customized by the user, who inputs
;two parameters, A and k. 'A' is a constant factor which changes where
;the function intersects the line y=1, in other words it determines
;the maximum phenotype value that will give a 100% chance of
;death. Meanwhile, k is a number which the argument of the function is
;divided by, and it changes how extreme the probability gradient is. 
;If k is close to zero than the probability will have a very
;steep falloff, i.e. the probability of death will dramatically
;decrease beyond a certain phenotype value. If k is larger, the
;probability gradient will be less steep.

;However, while A has no effect on the probability gradient, k DOES
;affect where the death probability function intersects y=1, so the
;two parameters are not completely independent. 

;After evaluating the death probability function, KILL generates a random
;number between 0 and 1 and compares it to this probability. 
;If the random number is between 0 and the probability of
;death, the organism "dies" and a 0 is returned. If not, the organism
;"survives" and a 1 is returned.

     deathprob=A*2.71^((-value/k))
     number=randomu(seed)

     if number LE deathprob then death=0 ;Dead
     if number GT deathprob then death=1 ;Survive
  return, death

end

function killinground_directional, generation, A, k
;This function takes in an array representing one generation's
;population, as well as the A and k parameters provided by the user,
;and evaluates the KILL_DIRECTIONAL funciton for each member of the
;population. It returns a new array of individuals that survive to reproduce.

  still_alive=[]
  foreach element, generation do begin
     if kill_directional(element, A, k) EQ 1 $ 
     then still_alive=[still_alive, element]
  endforeach

  return, still_alive
 
end



function offspring_directional, generation, A, k
;This function simulates the creation of a new generation with
;mutations. Each member of the population that was not killed off
;in the "killing round" produces one "offspring" element. A random
;number is generated and then fed into a mathematical function which
;determines how different the offspring's value will be from it's
;parent's value, i.e. how much the offspring will be "mutated". A new
;array is then created which includes all the offspring. 

 nextgen=[]
  foreach element, generation do begin
     if kill_directional(element, A, k) EQ 1 then begin
        x=randomu(seed)
        steplength=(1.3*x-.65)^3
        offspring=element+steplength
        nextgen=[nextgen, offspring]
     endif else begin
        nextgen=[nextgen]
     endelse
  endforeach
  return, nextgen
end



pro evolution_directional, n, A, k
  
  original_population=[randomn(seed, 10000)]
  generations=list(original_population)
  proportions=[]

  for i=0, n do begin
     if n_elements(generations[i]) LT 500 then begin
        currentgen=[generations[i], generations[i]]
     endif else begin
        currentgen=generations[i]
     endelse

     proportion_below_zero=n_elements(where(currentgen LT $
                          0))/float(n_elements(currentgen))
     proportions=[proportions, proportion_below_zero]
     population_after_killing=killinground_directional(currentgen, A, k)
     nextgen=offspring_directional(population_after_killing, A, k)
     generations.add, [nextgen]     

  endfor

  !P.MULTI=[0,3,3]

  for j=0,n, n/4 do begin
     histo=histogram(generations[j], locations=xvals, binsize=.1)
     plot, xvals, histo/total(histo), psym=10, charsize=2.5, $
           xtitle='Variety', ytitle='Frequency', title=[j], xrange=[-4,4]
     print, n_elements(generations[j])
  endfor

  plot, indgen(n)+1, proportions, charsize=2.5, xtitle='Generation', $
        ytitle='Frequency', title='Frequency of Phenotype Value <0', $
        color=!orange
  plot, indgen(n)+1, 1-proportions, charsize=2.5, xtitle='Generation', $
        ytitle='Frequency', title='Frequency of Phenotype Value >= 0', $
        color=!yellow
end 

















function kill_stabilizing, value, C

;This killing function uses an x^4 function to determine probability
;of death. The function is greater the farther the argument is from
;zero, hence this will tend to kill individuals whose values are far
;from zero, resulting in stabilizing selection.  

  deathprob=C*(value^2)
  number=randomu(seed)
  if number LE deathprob then death=0 ;Dead
  if number GT deathprob then death=1; Survive
  return, death

end


function killinground_stabilizing, generation, C

  still_alive=[]
  foreach element, generation do begin
     if kill_stabilizing(element,C) EQ 1 $ 
     then still_alive=[still_alive, element]
  endforeach
  return, still_alive
 
end


function offspring_stabilizing, generation, C

 nextgen=[]
  foreach element, generation do begin
     if kill_stabilizing(element, C) EQ 1 then begin
        x=randomu(seed)
        steplength=(1.3*x-.65)^3
        offspring=element+steplength
        nextgen=[nextgen, offspring]
     endif else begin
        nextgen=[nextgen]
     endelse
  endforeach
  return, nextgen

end



pro evolution_stabilizing, n, C
  
  original_population=[randomn(seed, 10000)]
  generations=list(original_population)
  proportions=[]

  for i=0, n do begin
     if n_elements(generations[i]) LT 500 then begin
        currentgen=[generations[i], generations[i]]
     endif else begin
        currentgen=generations[i]
     endelse

     proportion_abs_above_1=n_elements(where(abs(currentgen) GT 1))/float(n_elements(currentgen))
     proportions=[proportions, proportion_abs_above_1]
     population_after_killing=killinground_stabilizing(currentgen, C)
     nextgen=offspring_stabilizing(population_after_killing, C)
     generations.add, [nextgen]     

  endfor

  !P.MULTI=[0,3,3]

  for j=0,n, n/4 do begin
     histo=histogram(generations[j], locations=xvals, binsize=.1)
     plot, xvals, histo/total(histo), psym=10, charsize=2.5, $
           xtitle='Variety', ytitle='Frequency', title=[j], xrange=[-4,4]
     print, n_elements(generations[j])
  endfor

    plot, indgen(n)+1, proportions, charsize=2, xtitle='Generation', $
        ytitle='Frequency', title='Frequency of Phenotype of AbsValue>1', $
        color=!orange
  plot, indgen(n)+1, 1-proportions, charsize=2.5, xtitle='Generation', $
        ytitle='Frequency', title='Frequency of Phenotype Within 1 of 0', $
        color=!yellow

end


















function kill_disruptive, value, B, h

;This killing function uses a Gaussian distribution function to
;determine probability of death. Arguments far from zero will yield
;lower probabilities, hence this function will tend to kill
;individuals with phenotypes near zero, thus creating disruptive
;selection by  favoring extreme phenotypes. The constant 'h' controls
;the height of the graph, and thus the maximum probability of death. 'B'
;controls the steepness of the probability gradient.

  deathprob=h*(2.71)^(-(value^2)/B)
  number=randomu(seed)
  if number LE deathprob then death=0 ;Dead
  if number GT deathprob then death=1; Survive
  return, death

end

function killinground_disruptive, generation, B, h

  still_alive=[]
  foreach element, generation do begin
     if kill_disruptive(element,B, h) EQ 1 $ 
     then still_alive=[still_alive, element]
  endforeach
  return, still_alive
 
end


function offspring_disruptive, generation, B, h

 nextgen=[]
  foreach element, generation do begin
     if kill_disruptive(element, B, h) EQ 1 then begin
        x=randomu(seed)
        steplength=(1.3*x-.65)^3
        offspring=element+steplength
        nextgen=[nextgen, offspring]
     endif else begin
        nextgen=[nextgen]
     endelse
  endforeach
  return, nextgen

end


pro evolution_disruptive, n, B, h
  
  original_population=[randomn(seed, 10000)]
  generations=list(original_population)
  proportions=[]

  for i=0, n do begin
     if n_elements(generations[i]) LT 500 then begin
        currentgen=[generations[i], generations[i]]
     endif else begin
        currentgen=generations[i]
     endelse

     proportion_abs_above_1=n_elements(where(abs(currentgen) GT 1))/double(n_elements(currentgen))
     proportions=[proportions, proportion_abs_above_1]
     population_after_killing=killinground_disruptive(currentgen, B, h)
     nextgen=offspring_disruptive(population_after_killing, B, h)
     generations.add, [nextgen]     

  endfor

  !P.MULTI=[0,3,3]

  for j=0,n, n/4 do begin
     histo=histogram(generations[j], locations=xvals, binsize=.1)
     plot, xvals, histo/total(histo), psym=10, charsize=2.5, $
           xtitle='Variety', ytitle='Frequency', title=[j], xrange=[-4,4]
     print, n_elements(generations[j])
  endfor

    plot, indgen(n)+1, proportions, charsize=2, xtitle='Generation', $
        ytitle='Frequency', title='Frequency of Phenotype of AbsValue>1', $
        color=!orange
    plot, indgen(n)+1, 1-proportions, charsize=2.5, xtitle='Generation', $
        ytitle='Frequency', title='Frequency of Phenotype Within 1 of 0', $
        color=!yellow

end
