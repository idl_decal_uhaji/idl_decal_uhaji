;==========RADIANS TO DEGREES==========

function R_T_D, radians

  ;converts radians to degrees

  return, (radians)*(180/3.1415926535)

end

;==========DEGREES TO RADIANS==========

function D_T_R, degrees

  ;converts degrees to radians

  return, (degrees)*(3.14159265/180)

end

;==========WEIGHT ON MARS==========

function weight_on_mars, mass_in_kg

  ;takes your mass in kg, gives you your weight on mars in pounds

  return, (mass_in_kg)*(3.75)*(.2248)

  ;Acceleration near surface of Mars = 3.75 m/s^2
  ;1 Newton = .2248 pounds

end

;==========TIP CALC==========

function tip_calc, amount_of_check, group_size

  ;returns tip amount, total+tip, and cost per person

  Tip=(amount_of_check*.15)
  Total=(amount_of_check)+(Tip)
  Cost_per_person=(Total/group_size)

  Tip_array=[Tip, Total, Cost_per_person]

  return, Tip_array
  
end

;==========SWAP 'EM==========

function SWAP_EM, a, b

  ;takes two values and returns them in swapped order

  Swapped_values=[b, a]

  return, swapped_values

end

;==========Nth ROOT==========

function Nth_ROOT_UPGRADED, n, x

  ;returns the nth root of x, only valid for positive x

   if ISA(n, /NUMBER) EQ 0 || ISA(x, /NUMBER) EQ 0 then begin
   ;This checks that the inputs are numbers.
        return, 'NUMBERS ONLY, BOZO'
   endif else begin
  
      if n EQ 0 then begin
      return, 'ZEROTH ROOT IS UNDEFINED'
   
      endif else begin

         if x LT 0 then begin
            print, 'NO NEGATIVE RADICANDS, BOZO'

         endif else begin
            a=FLOAT(x)
            b=FLOAT(n)
            c=a^(1/b)
            return, c
         
         endelse

      endelse

   endelse
   
end
