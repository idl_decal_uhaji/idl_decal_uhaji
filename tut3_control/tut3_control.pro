
pro SQUAREROOTS, a, b

;This procedure takes two positive integers and prints the square
;roots of all integers between them, inclusively.

  if (ISA(a, 'Int') EQ 1) && (ISA(b, 'Int') EQ 1) && (a LT b) && (a GT 0) && (b GT 0) then begin
                                ;The above line checks that the inputs
                                ;are positive integers, and that b>a.
;;;;;;;;;;;;;;;;;;;;;;Should also check data type (-10);;;;;;;;;;;;;;;;;
         for i=a, b do begin
            print, SQRT([i])

         endfor

  endif else begin
         print, 'ERROR: YOU MUST ENTER TWO POSITIVE INTEGERS, AND THE SECOND MUST BE GREATER THAN THE FIRST.'

  endelse

end





pro FACTORIAL, a

;This procedure calculates the factorial of any integer from 1 to 12
 
     if (ISA(a, 'Int') EQ 1) && (a GE 1) && (a LE 12) then begin   
     ;This checks that the input is a positive integer between 1 and 12, inclusively.
;;;;;;;;;;;;;;;;;;;ditto above;;;;;;;;;;;;;;;;;;;;;;;;
        factorial_array=[]

        for i=1, a do begin
            factorial_array=[factorial_array, i]
            ;The strategy is to add all integers
            ;between 1 and a to an array, inclusively,
            ;and then take the product of all those numbers.

        endfor

        print, product(factorial_array)

     endif else begin
        print, 'ERROR: YOU MUST ENTER A POSITIVE INTEGER FROM 1 to 12.'

     endelse

end





pro ARRAYLOOPCONDITIONAL

;This no-input procedure takes elements from an automatically self-generated array
;and puts all elements >= 50 into a new array.

  original_array=randomu(seed, 1000)*100
  ;This creates a random 1000-element array.

   new_array=[]
   for i=0, 999 do begin
      If original_array(i) GE 50.0 then begin
      new_array=[new_array, original_array(i)]
      ;Each element >= 50 gets added to the new array.

      endif

   endfor

  print, new_array

end





pro ARRAYLOOPCONDITIONAL_V2

;An alternative way of doing the above procedure, using a technique I
;found online. I left this in here for my reference.

  original_array=randomu(seed, 1000)*100
  ;This creates a random 1000-element array.

   new_array=[]
   foreach element, original_array do begin
      If element GE 50.0 then begin
      new_array=[new_array, element]
      ;Each element >= 50 gets added to the new array.

      endif

   endforeach

  print, new_array

end





pro ARRAYWHERE

;This no-input procedure uses the a WHERE function to do the same
;thing as above, in a much easier way

  original_array=randomu(seed, 1000)*100
  ;This creates a random 1000-element array.

  new_array=WHERE(original_array GT 50.0)
  
  print, original_array[new_array]
  ;The WHERE function only gives us the
  ;subscripts of the original array where
  ;it is greater than 50. So we need to 
  ;print the original array evaluated at
  ;those locations.
 
end







