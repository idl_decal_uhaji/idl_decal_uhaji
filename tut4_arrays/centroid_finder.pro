;This code finds the centroids of the three most prominent peaks in
;the spectrum data  found in spectra.txt. It does this in the following way:
;
;First, to eliminate noise, it sets an intensity floor; it finds the
;mean and standard deviation of the intensity values, and then any
;value that is less than two standard deviations above the mean intensity
;is set to zero. What we are left with is an array with mostly zeros
;but several discrete peak regions. with the intesity values preseved.

;Next, the indices of peak regions are found, and these indices are 
;separated into new arrays based on contiguity of the indices.
;This allows us to separate all the peak regions into their own 
;arrays and analyze them separately. 

;Next, each of the arrays (one for each peak region) is analyzed to
;find a weighted average of the x-values in the region; each x-value
;(corresponding to a particular wavelength) is multiplied by the intensity value at
;the wavelength, divided by the total intensity at all wavelengths in
;the region. The sum of all these weighted x-values gives the weighted
;average wavelength for the peak region. 

;Finally, the script prints an array which contains the x-values of
;the centroids for all three peak regions.

;The code is written in a modular form, so the entire script can be
;run by simply executing the "MAIN" procedure.

function load_spec
  
  readcol, 'spectra.txt', spectrax, spectray, format='F,F', numline=2048, skipline=17
  
  ret_arr=[[spectrax],[spectray]]
  
  return, ret_arr

end



function spectra_y_values

;This function takes the output of load_spec and returns an array
;consisting only of the y-values from the output array of
;load_spec. Thus we end up with only the y-values i.e. the intensity
;of the quasar

  spectra_array=load_spec()

  y_values=spectra_array[*,1]

  return, y_values

end



function spectra_x_values

;This is similar to the above function except it creates an array of
;x-valiues (corresponding to the wavelength values).

  spectra_array=load_spec()

  x_values=spectra_array[*,0]

  return, x_values

end



function spectra_mean

;This procedure finds the mean of the y-values
;i.e. the average intensity. We use this in a later procedure to
;eliminate noise in the data.

  return, mean(spectra_y_values())

end



function spectra_stddev

;This procedure finds the standard deviation of the y-values, which
;will also be used in elminating noise.

  return, stddev(spectra_y_values())

end



function noiseless_data
  
;This function eliminates noise from the data by going through all the
;spectra y values and changing them to 0 if they are less than 2
;standard deviations above the mean. The result is an array of
;noiseless y values, where only the ones that were extremely high are
;preserved. The rest are zeros.

  noiseless_data=[] ;Define an empty array to fill with the new values.

  y=spectra_y_values()

  d=(spectra_mean())+(2*(spectra_stddev())) ;This is the cutoff point for a value to not be discarded.

  foreach element, y do begin
     
     if element  LT d then begin

        noiseless_data=[noiseless_data, 0]

        endif else begin

           noiseless_data=[noiseless_data, element]

        endelse

     endforeach

  return, noiseless_data

end

function nonzero_element_indices
  
;Gives the indexs where the noiseless spectrum is not zero (i.e. the
;locations where the actual notable peaks are).

  y=noiseless_data()

  array=[]

  foreach element, y do begin

  if element NE 0 then begin
           
        array=[array, where(y EQ element)]
      
        endif

  endforeach

  return, array

end


function peakregion1
  
;This is the first of three functions that split the array of indices
;into contiguous sections. This is done so that the intensity values
;from each peak are clumped together into their own array. 

  y=nonzero_element_indices()

  n=n_elements(y)

  peakregion1=[y(0)]

  for i=0, n-1 do begin
     
     if  y(i+1) EQ (y(i)+1) then begin

        peakregion1=[peakregion1,y(i+1)]
     
     endif else break

  endfor  

  return, peakregion1 ;This output is an array of indices corresponding to the noiseless data (and also to the original data, since indexing was preserved in the noise elimimination process). This range of values tells us the location of the peak.
 
end



function peakregion2

  y=nonzero_element_indices()
  peakregion1=[peakregion1()]
  n=n_elements(y)
  start=n_elements(peakregion1)

  peakregion2=[y(start)]

  for i=start, n-1 do begin

     if y(i+1) EQ y(i)+1 then begin

        peakregion2=[peakregion2, y(i+1)]

     endif else break
  
  endfor

  return, peakregion2

end



function peakregion3

  y=nonzero_element_indices()
  peakregion1=[peakregion1()]
  peakregion2=[peakregion2()]
  n=n_elements(y)
  z=[peakregion1, peakregion2]
  start=n_elements(z)

  peakregion3=[]

  for i=start, n-1 do begin

       peakregion3=[peakregion3, y(i)]
  
  endfor

  return, peakregion3

end



function centroid_peakregion1

;This function and the two following functions find the wavelength
;(i.e. the spectrum x-value) at which the centroid of the peak is
;located. It does this by taking a weighted average of the wavelengths
;based on their intensity. For each wavelength value in the input
;array, the function takes this wavelength value and multiplies it by
;the intensity at that wavelength divided by the total intensity
;measured at all wavelengths in the peak region in which that
;wavelength is found. This allows us to quantify the
;contribution of each wavelength to the total intensity in the peak
;region, instead of just crudely averaging the wavelengths (the
;x-values) together. The sum of these weighted x-values gives us the
;average wavelength across the entire peak region. This value is the x-value where the
;centroid is located in the original spectrum data.

  x=peakregion1()
  y=noiseless_data()
  intensity=y[x] ;Remember that the array x is full of indices, not intensity values. We must evaluate (at x) the array corresponding to the data in order to find the actual intensity at each point in the peak region.

  weighted_x_values=[]

  for i=0, n_elements(x)-1 do begin

     weighted_x_values=[weighted_x_values, x(i)*(intensity(i)/total(intensity))]

  endfor

  weighted_avg=(total(weighted_x_values))

  return, weighted_avg ;This is the wavelength where the centroid is located (we are only concerned with the x-position of the centroid, hence we only give a wavelength and not an intensity).

end


function centroid_peakregion2

  x=peakregion2()
  y=noiselesS_data()
  intensity=y[x]

  weighted_x_values=[]

  for i=0, n_elements(x)-1 do begin
     
     weighted_x_values=[weighted_x_values, x(i)*(intensity(i)/total(intensity))]

  endfor

  weighted_avg=total(weighted_x_values)

  return, weighted_avg

end


function centroid_peakregion3

  x=peakregion3()
  y=noiseless_data()
  intensity=y[x]

  weighted_x_values=[]

  for i=0, n_elements(x)-1 do begin

     weighted_x_values=[weighted_x_values, x(i)*(intensity(i)/total(intensity))]

  endfor

  weighted_avg=total(weighted_x_values)

  return, weighted_avg

end


pro main

;This runs the entire collection of modules and thereby prints the wavelengths where the centroids of the three most
;prominent peaks are located.

  print, [centroid_peakregion1(), centroid_peakregion2(), centroid_peakregion3()]
  
end
