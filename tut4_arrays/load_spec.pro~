function load_spec
  
  readcol, 'spectra.txt', spectrax, spectray, format='F,F', numline=2048, skipline=17
  
  ret_arr=[[spectrax],[spectray]]
  
  return, ret_arr

end



function spectra_y_values

;This function takes the output of load_spec and returns an array
;consisting only of the y-values from the output array of
;load_spec. Thus we end up with only the y-values i.e. the intensity
;of the quasar

  spectra_array=load_spec()

  y_values=spectra_array[*,1]

  return, y_values

end



function spectra_x_values

  spectra_array=load_spec()

  x_values=spectra_array[*,0]

  return, x_values

end



function spectra_mean

;This procedure finds the mean of the y-values
;i.e. the average intensity. We use this in a later procedure to
;eliminate noise in the data.

  return, mean(spectra_y_values())

end



function spectra_stddev

;This procedure finds the standard deviation of the y-values, which
;will also be used in elminating noise.

  return, stddev(spectra_y_values())

end



function denoise
  
;This function eliminates noise from the data by going through all the
;spectra y values and changing them to 0 if they are less than 2
;standard deviations above the mean. The result is an array of
;noiseless y values, where only the ones that were extremely high are
;preserved. The rest are zeros.

  noiseless_data=[] ;Define an empty array to fill with the new values.

  y=spectra_y_values()

  d=(spectra_mean())+(2*(spectra_stddev())) ;This is the cutoff point for a value to not be discarded.

  foreach element, y do begin
     
     if element  LT d then begin

        noiseless_data=[noiseless_data, 0]

        endif else begin

           noiseless_data=[noiseless_data, element]

        endelse

     endforeach

  return, noiseless_data

end

function nonzero_element_indices

;Gives the indexs where the denoised spectrum is not zero.

  y=denoise()

  array=[]

  foreach element, y do begin

  if element NE 0 then begin
           
        array=[array, where(y EQ element)]
      
        endif

  endforeach

  return, array

end


function array_splitter
  
;This splits the array into contiguous pieces based on the indices of
;the y-values. This is done in order to separate the peak regions.

  y=nonzero_element_indices()

  n=n_elements(y)

  array_peak=[]

  for i=1, n-1 do begin
     
     if  y(i) EQ (y(i-1)+1) then begin

        array_peak=[array_peak,y(i)]

     endif

  endfor

  return, array_peak


end
