;Debugging Homework

;;;NEED TO UNCOMMENT CODE AND ACTUALLY TEST IT. -10

;pro error1
;   x = findgen(10)
;   for i = 0, n_elements(x) do begin ;;; Need to have n_elements -1 for
;   proper indexing. -5
;       print, x[i]
;   endfor
;end

;The above code creates an array of 10 floating integers from 0.0 to
;9.0 and then prints each element in the array. There were multiple
;problems with this code. First, 'endfor' was spelled wrong. Secondly,
;there was no comma after 'print'. Finally, instead of "i = 0,
;n_elements(x)" it should read, "i = 0, n_elements(x-1)"
;because even though there are 10 elements in the array, the last
;subscript is 9 since the index starts at 0. The code will still work
;completely fine if you just leave it the way it is, but it will give
;you an error message at the end saying the index is out of range
;(because there is no element in the array with a subscript of 10).





;pro error2
;   print, "hello"
;end

;This code simply prints the word "hello". There were two spelling
;errors in this code: "pro" and "print" were spelled wrong. Also,
;"hello" was not in quotation marks.





;pro error3
;   a='ed'
;   b=' is'
;   c=' a girl?'
;   result=a+b+c
;   print, result
;end

;This code defines three variables and then prints them in order,
;creating a sentence. The first problem with this code was that it was
;a function yet it said "print" at the end. Print is for procedures
;and return is for functions. So I changed it to a procedure. Also,
;the "result" included an undefined variable "d", so I changed it to
;say, "result=a+b+c". Finally, there were extra spaces included in the
;definitions of the variables, which I got rid of.





;pro error4
;   x = ['1','2','3']
;   y = ['a','b','c']
;   z = [x, y]
;   print, z
;end

;This code defines two arrays and then creates a new array that is a
;concatenation of the two. The problem with this code is that arrays
;can only contain one data type, so you can't combine an array
;of integers with an array of letters, at least not the way this code
;attempted to do it. To fix this, I defined all the elements in x as
;strings by added quotation marks around them. This way all the
;elements in x and y are strings, so the two can be
;concatenated. Also, I added a command to print the new array.





;pro error5
;   x=findgen(100,100)
;   s=size(x)
;   for i=0,s[1] do begin
;      for j=0,s[2] do begin
;        if (i+j) EQ 90 then begin
;           x[i,j]=0
;        endif
;      endfor
;   endfor
;   for i=0,s[1]-1 do begin
;      for j=0,s[2]-1 do begin
;         if (i+j) GT 45 then begin
;           x[i,j]=i+j
;         endif
;      endfor 
;   endfor
;   print,x
;end

;
;First, this code had atrocious formatting, so the first thing I did
;was indent everything properly. Next, the second and fourth for loops
;weren't ended properly, so I added the proper syntax to end
;them. Furthermore, the proper syntax for "greater than" and "equal to" was not used,
;so I changed "i+j>45" to "(i+j) GT 45" and "i+j=90" to "(i+j) EQ
;90". Finally, the line that read "x[i, j[=0" was changed to "x[i,j]=0".






























































































;There are no easter eggs down here, go away.










































;The solution to Homework 2 can be found at...




































































;Haha, got ya
