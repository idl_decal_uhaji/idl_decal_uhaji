
;;;Missing post script file. -5
;;; Look into the run error.

pro smile

;First we plot a circle. The first part of the strategy is to create
;an array of x-values that is effectively continuous, by making the
;spaces between them very small. We do this by dividing the maximum
;desired x value by a huge number and then multiplying by each element
;in an equally huge integer array to slowly make the x values bigger
;and bigger [DISCLAIMER: I did not come up with this technique. I
;found the idea online.]

;After generating our x values, we evaluate sine and cosine at each x
;value to get the x and y values for the circle.

  x_vals=(2*!Pi/9999)*findgen(10000)
  head_x=1+cos(x_vals)
  head_y=1+sin(x_vals)

  plot, head_x, head_y, title='How I Felt When I Finished Coding This', charsize=2, color=!yellow, /ISOTROPIC ;The isotropic keyword forces a 1:1 aspect ratio
  
;Next we plot the smile, which is a semicircle of radius .75. We use a
;similar technique as before to get an effectively continuous set of x
;values that ends at the right bound of the semicircle (i.e. the
;center plus the radius, or 1+.75)

  smile_x_vals=float((1.75/9999.)*findgen(10000))
  smile_y=float(-sqrt(.75^2-(smile_x_vals-1)^2)+1) ;The equation for a concave-up semicircle

  oplot, smile_x_vals, smile_y, color=!orange


;Now we do the left eye. Here we need the equation for an ellipse. We
;choose some values for our semimajor axis, semiminor axis, vertical
;shift, and horizontal shift. The explicit form of an ellipse (i.e. in
;'y=' form) requires two equations, one each for y±√(stuff). One
;equation corresponds to the top half of the ellipse, the other for
;the bottom half.


  a=.2 ;Semimajor axis
  b=.1 ;Semiminor axis
  k=1.25 ;Vertical shift
  h=.75 ;Horizontal shift

  left_x_vals=float(((.75+b)/9999.)*findgen(10000)) ;The x values for the left eye

  left_y_vals_bottom=float(k-sqrt((a^2)*(1.-(left_x_vals-h)^2/(b^2)))) ;The bottom half of the eye

  left_y_vals_top=float(k+sqrt((a^2)*(1.-(left_x_vals-h)^2/(b^2)))) ;Top half

  oplot, left_x_vals, left_y_vals_bottom, color=!blue
  oplot, left_x_vals, left_y_vals_top, color=!blue

;Now we do the right eye, which is really easy because all we have to
;do is take the x values for the left eye, shift them over to the
;right, and plot them with the same y values.

  oplot, left_x_vals+.5, left_y_vals_bottom, color=!blue
  oplot, left_x_vals+.5, left_y_vals_top, color=!blue

;And we're done!

end
