;Was not able to complete this tutorial because after hours of
;searching I could not figure out how to use the display procedure to
;zoom in and display only a section of the whole image. Turns out the
;documentation on the display procedure is incredibly bad because it
;is not a native IDL function, and our textbook does not discuss this
;either.
;So this is as far as I got. Hopefully I can get some partial credit.

;;;The way to do this is to change the image size, not zoom in with the
;display function
;;;Run time error. -5

function whats_my_name, xpos, ypos

;This function is really a combination of mindist and whats_my_name.
;When I wrote mindist I originally set it up to return the name of the
;kitten, and then I realized mindist was only supposed to find the
;kitten and whats_my_name was suposed to return it. I didn't
;see the point in having two separate functions for this so I just
;combined them into one.

  img=mrdfits('idl_image.fits', 0, header);;;incorrect file path. -5

   kit1loc=[150, 400]
   kit2loc=[700, 700]
   kit3loc=[600, 300]
   kit4loc=[1000, 600]

   kits=['Molly', 'Mary', 'Mike', 'Malakai'] ;;;USe the header. -5
   
   ;Below, we define the distances from the given position to
   ;each of the kitten's faces. Then we simply use 
   ;Pythagorean theorem to find the distance between the point
   ;and each face.
      
   xdist1=float(xpos-kit1loc(0))
   ydist1=float(ypos-kit1loc(1))

   xdist2=float(xpos-kit2loc(0))
   ydist2=float(ypos-kit2loc(1))

   xdist3=float(xpos-kit3loc(0))
   ydist3=float(ypos-kit3loc(1))

   xdist4=float(xpos-kit4loc(0))
   ydist4=float(ypos-kit4loc(1))
   
   kit1dist=SQRT((xdist1)^2+(ydist1)^2)
   kit2dist=SQRT((xdist2)^2+(ydist2)^2)
   kit3dist=SQRT((xdist3)^2+(ydist3)^2)
   kit4dist=SQRT((xdist4)^2+(ydist4)^2)
   
   distances=[kit1dist, kit2dist, kit3dist, kit4dist]

   return, kits[where(distances EQ min(distances))] 

   ;Here we simply take the minimum value of the distances
   ;and evaluate the array containing the names of the kittens
   ;at that index value.
 
end


;;;Missing: main, -10, colorzoomn, -10, better_half, -10
