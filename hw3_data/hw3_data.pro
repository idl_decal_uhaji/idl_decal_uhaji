function load_spec

  readcol, 'spectra.txt', skipline=17, pixels, intensity

  return, [[pixels],[intensity]]

end



pro spectraplot
;;;;;;;;;;;;;;;;code misses one of the peaks (-15);;;;;;;;;;;
  array=load_spec()

  x_vals=array[*,0]
  y_vals=array[*,1]

  ;We set the x values equal to everything in the first row of 
  ;load_spec, and the y values as everything in the second row.

  plot, x_vals, y_vals, psym=6, title='Spectrum Data', xtitle='Pixels', ytitle='Intensity', charsize=2, color=!black, background=!white, symsize=.5, xrange=[0,2100]
  
  ;In order to create a vertical line, we define two
  ;points at the x-value corresponding to the centroid: one point
  ;is at y=0 and another one is at the top of the graph. When
  ;we plot the two points they automatically get connected by 
  ;a line. We do this for all three of our centroid points.

  centroid1_xval=[483.365, 483.365]
  centroid2_xval=[1107.53, 1107.53]
  centroid3_xval=[1533.65, 1533.65]

  centroid_yval=[0, 4000]

  oplot, centroid1_xval, centroid_yval, linestyle=2, color=!blue
  oplot, centroid2_xval, centroid_yval, linestyle=2, color=!blue
  oplot, centroid3_xval, centroid_yval, linestyle=2, color=!blue

end



pro gen_gauss

;This function generates three Gaussian distributions and plots
;histograms of them,

;Below is the code for the first distribution.

  ;First we generate an array of random, normally
  ;distributed values. Then we assign a standard deviation
  ;and mean for it. Then we create a histogram with the 
  ;appropriate bin size and plot it.

  array1=randomn(seed, 1000)
  sigma1=7
  mean1=25
  gauss1=array1*sigma1+mean1
  histo1=histogram(gauss1, binsize=3, locations=x1)

  plot, x1, histo1, psym=10, xrange=[0,150], yrange=[0,200], color=!white, xtitle='Value', ytitle='Frequency', title='Gaussian Distributions', charsize=2

;Below is the code for the second distribution.

  array2=randomn(seed, 1000)
  sigma2=5
  mean2=70
  gauss2=array2*sigma2+mean2
  histo2=histogram(gauss2, locations=x2, binsize=2)

  oplot, x2, histo2, psym=10, color=!orange

;Below is the code for the third distribution

  array3=randomn(seed, 1000)
  sigma3=9
  mean3=120
  gauss3=array3*sigma3+mean3
  histo3=histogram(gauss3, binsize=2, locations=x3)

  oplot, x3, histo3, psym=10, color=!blue

end

