pro clues

;This procedure pieces together a string from strings read from clues.txt.
  
  ;First we read the strings from clues.txt, put them
  ;into an array, and define an empty array into which
  ;we will put each element that will form the name
  ;of the file we are looking for.
  
  readcol, 'clues.txt', clues, Format='A'
  clues=[clues]
  new_array=[]

;Finding the first element
  
  ;We take the first letter of the first string and make
  ;it lowercase.
  element1=strlowcase(strmid(clues[0], 0, 1))
  new_array=[new_array, element1]

;Finding the second element

  ;We search all strings for the substring 'og' and then 
  ;extract 'og' from it.
  foreach element, clues do begin
     if strmatch(element, '*og*') EQ 1 then begin
        element2=strmid(element, strpos(element, '*og*'), 2)

        new_array=[new_array, element2]

     endif
  endforeach

;Finding the third element

   element3='a_'
   new_array=[new_array, element3]

;Finding the fourth element

  foreach element, clues do begin
     if strmatch(element, '*ate*') EQ 1 then begin
         element4=strmid(element, strpos(element, 'p'), 3)

         new_array=[new_array, element4]

     endif
  endforeach
         
;Finding the fifth element

  ;We search for strings with the letter x in them, and 
  ;then take the first 2 letters of the string. To avoid
  ;doing this twice, we break the loop after one string
  ;with 'x' is found.
   foreach element, clues do begin
      if strmatch(element, '*x*') EQ 1 then begin
         element5=strmid(element, 0, 2)

         new_array=[new_array, element5] 

      break
      endif
   endforeach

;Replacing the second 'o'
     
   new_array=strjoin(new_array)

   print, repstr(new_array, 'po', 'p')
  ;I spent hours searching online for any documentation
  ;about REPSTR keywords that would enable me to replace
  ;one 'o' and not the other. No such documentation was
  ;found, so I resorted to this quick and dirty solution.

end
